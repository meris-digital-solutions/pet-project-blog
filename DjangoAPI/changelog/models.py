from django.db import models
from django.contrib.auth import get_user_model

from users.models.user import User

class Changelog(models.Model):
    created_by = models.ForeignKey(
        User,
        verbose_name="Автор",
        on_delete=models.CASCADE, null=True)
    title = models.CharField("Заголовок", max_length=100, null=True)
    text = models.TextField("Описание", null=True)
    created_at = models.DateTimeField("Дата создания", auto_now_add=True, null=True)
    updated_at = models.DateTimeField("Дата обновления", auto_now=True, null=True)

    class Meta:
        verbose_name = "Логи"
        verbose_name_plural = "Логи"


    def __str__(self):
        return self.title