from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions, generics, viewsets
from changelog.serializers import ChangeLogSerializer
from changelog.models import Changelog


@permission_classes((permissions.AllowAny,))
class ChangeLogView(viewsets.ModelViewSet):
    serializer_class = ChangeLogSerializer
    queryset = Changelog.objects.all()
