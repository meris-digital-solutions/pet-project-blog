from rest_framework import serializers
from changelog.models import Changelog


class ChangeLogSerializer(serializers.ModelSerializer):
    created_by = serializers.HiddenField(default=serializers.CurrentUserDefault())
    class Meta:
        model = Changelog
        fields = ('id',
                  'title',
                  'text',
                  'created_at',
                  'updated_at',
                  'created_by',
                  )