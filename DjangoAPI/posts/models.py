from django.db import models
from users.models.user import User


class Article(models.Model):
    created_by = models.ForeignKey(
        User,
        verbose_name="Автор",
        on_delete=models.CASCADE)
    title = models.CharField("Заголовок", max_length=100)
    text = models.TextField("Описание")
    created_at = models.DateTimeField("Дата создания", auto_now_add=True)
    updated_at = models.DateTimeField("Дата обновления", auto_now=True)

    class Meta:
        verbose_name = "Статья"
        verbose_name_plural = "Статьи"

    @property
    def likes(self):
        return self.grades.filter(is_liked=True).count()

    @property
    def dislikes(self):
        return self.grades.filter(is_liked=False).count()

    def __str__(self):
        return self.title

class Grade(models.Model):
    is_liked = models.BooleanField(
        default=True,
    )
    article = models.ForeignKey(
        'Article',
        on_delete=models.CASCADE,
        verbose_name="Пост",
        related_name="grades",
        blank=True,
        null=True
    )
    created_by = models.ForeignKey(User, verbose_name="Автор", on_delete=models.CASCADE)
    created_at = models.DateTimeField("Дата создания", auto_now_add=True)
    updated_at = models.DateTimeField("Дата обновления", auto_now=True)

    class Meta:
        verbose_name = "Оценка"
        verbose_name_plural = "Оценки"
