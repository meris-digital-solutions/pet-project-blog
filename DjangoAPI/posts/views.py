from django.shortcuts import render
from django.http import JsonResponse

from rest_framework.decorators import api_view, permission_classes, action
from rest_framework import permissions, generics, viewsets
from rest_framework.response import Response
from posts.serializers import ArticleSerializer, ArticleSerializerAuthorized, GradeArticleSerializer, ArticleSerializerUpdate

from posts.models import Article
from users.models.user import User
from utils.models.check_authorization import CheckPostUser


class ArticleView(viewsets.ModelViewSet):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()

    permission_classes = [permissions.IsAuthenticated, CheckPostUser]
    def get_serializer_class(self):
        serializer = self.serializer_class
        if self.action == 'create':
            serializer = ArticleSerializerAuthorized
        elif self.action == 'update':
            serializer = ArticleSerializerUpdate
        elif self.action == 'like':
             serializer = GradeArticleSerializer

        return serializer

    @action(methods=['POST'], detail=True, url_path='like')
    def like(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.grades.create(is_liked=True, created_by=self.request.user)
        return Response(status=200)

