from rest_framework import serializers

from posts.models import Article, Grade
from changelog.models import Changelog

class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = '__all__'
        #
        fields = ('id',
                    'title',
                    'text',
                    'created_at',
                    'updated_at',
                    'created_by',
                   'likes',
                    'dislikes',
        )

class ArticleSerializerAuthorized(serializers.ModelSerializer):
    created_by = serializers.HiddenField(default=serializers.CurrentUserDefault())
    class Meta:
        model = Article
        fields = ('id',
                  'title',
                  'text',
                  'created_at',
                  'updated_at',
                  'created_by',
                  'likes',
                  'dislikes',
                  )

class ArticleSerializerUpdate(serializers.ModelSerializer):
    created_by = serializers.HiddenField(default=serializers.CurrentUserDefault())
    class Meta:
        model = Article
        fields = ('id',
                  'title',
                  'text',
                  'created_at',
                  'updated_at',
                  'created_by',
                  )

    def update(self, instance, validated_data):
        Changelog.objects.create(title=instance.title,
                                text=instance.text,
                                created_by=instance.created_by,)
        return super().update(instance, validated_data)

class GradeArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Grade
        fields = (
            'id',
            'created_at',
            'updated_at',
            'created_by',
            'is_like',
            'post',
        )
