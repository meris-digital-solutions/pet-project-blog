from django.urls import path, include
from rest_framework import routers, serializers, viewsets
from django.contrib import admin
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
# from users.models import User

from users.views import UserView
from posts.views import ArticleView
from comments.views import CommentsView
from changelog.views import ChangeLogView

router = routers.DefaultRouter()
router.register('users', UserView)
router.register('article', ArticleView)
router.register('comments', CommentsView)
router.register('log', ChangeLogView)



urlpatterns = [
    path('', include(router.urls)),
    # path('api/', include('main.urls')),
    # path('admin/', admin.site.urls),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
