from django.shortcuts import render
from django.http import JsonResponse

from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from rest_framework.response import Response


class CheckPostUser(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.created_by == request.user


class CheckUser(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.id == request.user.id


# class CheckPostUser(permissions.BasePermission):
#     def has_object_permission(self, request, view, obj):
#         return obj.created_by == request.user
