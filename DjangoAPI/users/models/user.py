from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User, AbstractBaseUser
from users.models.managers import UserManager


# from utils.models import AbstractUUID, AbstractTimeTrackable
from users.models.user import User

# User = get_user_model()

class User(AbstractBaseUser):
    GENDER = (
        ('Мужской', 'Мужской'),
        ('Женский', 'Женский')
    )

    email = models.EmailField("Почта", unique=True, max_length=254, default='')
    username = models.CharField("Логин", unique=True, max_length=32, default='')
    is_active = models.BooleanField("Активный пользователь", default=True)
    is_admin = models.BooleanField("Админ", default=False)

    first_name = models.CharField("Имя", max_length=50, null=True)
    last_name = models.CharField("Фамилия", max_length=50, null=True)
    first_login = models.DateTimeField("Дата регестрации", auto_now_add=True)
    phone = models.CharField("Телефон", max_length=14, blank=True)
    birthday = models.DateField("Дата рождения", blank=True, null=True)
    gender = models.CharField("Пол", max_length=7, choices=GENDER, default='Женский')
    is_staff = models.BooleanField(
        default=False
    )
    is_superuser = models.BooleanField(
        default=False
    )
    is_active = models.BooleanField(
        default=True
    )

    objects = UserManager()

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

