from django.shortcuts import render
from django.http import JsonResponse

from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions, generics, viewsets
from rest_framework.response import Response
from .serializers import UserSerializer, UserSerializerUpdate

from users.models.user import User
from utils.models.check_authorization import CheckPostUser, CheckUser
# from rest_framework.permissions import AllowAny, IsAdminUser

@permission_classes((permissions.AllowAny,))
class UserView(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    # permission_classes_by_action = {'create': [AllowAny],
    #                                 'update': [IsAdminUser]}

    def get_serializer_class(self):
        serializer = self.serializer_class
        if self.action == 'create':
            serializer = UserSerializer
        elif self.action == 'update':
            serializer = UserSerializerUpdate

        return serializer
