from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User, AbstractBaseUser

from posts.models import Article
from users.models.user import User

# User = get_user_model()


class Comment(models.Model):
    created_by = models.ForeignKey(
        User,
        verbose_name="Автор",
        on_delete=models.CASCADE)
    post_at = models.ForeignKey(
        Article,
        verbose_name="Новость",
        on_delete=models.CASCADE,
        default="",
        null=True)
    text = models.TextField("Описание", max_length=512, default="")
    created_at = models.DateTimeField("Дата создания", auto_now_add=True)
    updated_at = models.DateTimeField("Дата обновления", auto_now=True)

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"

    @property
    def likes(self):
        return self.grades.filter(is_liked=True).count()

    @property
    def dislikes(self):
        return self.grades.filter(is_liked=False).count()

    def __str__(self):
        return self.text


class Grade_comment(models.Model):
    is_liked = models.BooleanField(
        default=True,
    )
    article = models.ForeignKey(
        'Comment',
        on_delete=models.CASCADE,
        verbose_name="Пост",
        related_name="grades",
        blank=True,
        null=True
    )
    created_by = models.ForeignKey(User, verbose_name="Автор", on_delete=models.CASCADE)
    created_at = models.DateTimeField("Дата создания", auto_now_add=True)
    updated_at = models.DateTimeField("Дата обновления", auto_now=True)

    class Meta:
        verbose_name = "Оценка"
        verbose_name_plural = "Оценки"
