from rest_framework import serializers
from comments.models import Comment, Grade_comment


class CommentsSerializer(serializers.ModelSerializer):
    created_by = serializers.HiddenField(default=serializers.CurrentUserDefault())
    class Meta:
        model = Comment
        fields = '__all__'


class GradeCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Grade_comment
        fields = (
            'created_at',
            'updated_at',
            'created_by',
            'is_like',
            'post',
        )