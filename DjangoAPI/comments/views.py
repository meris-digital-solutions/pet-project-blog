from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions, generics, viewsets
from rest_framework.response import Response
from .serializers import CommentsSerializer
from comments.models import Comment


@permission_classes((permissions.IsAuthenticated,))
class CommentsView(viewsets.ModelViewSet):
    serializer_class = CommentsSerializer
    queryset = Comment.objects.all()

    # permission_classes = [permissions.IsAuthenticated, CheckPostUser]

    def get_serializer_class(self):
        serializer = self.serializer_class
        if self.action == 'create':
            serializer = CommentsSerializer
        elif self.action == 'update':
            serializer = CommentsSerializer

        return serializer

# @permission_classes((permissions.AllowAny,))
# class CommentsView(generics.CreateAPIView):
#     serializer_class = CommentsSerializer
#
#
# @permission_classes((permissions.AllowAny,))
# class CommentsListView(generics.ListAPIView):
#     serializer_class = CommentsSerializer
#     queryset = Comment.objects.all()
#
#
# @permission_classes((permissions.AllowAny,))
# class CommentsDetailView(generics.RetrieveUpdateDestroyAPIView):
#     serializer_class = CommentsSerializer
#     queryset = Comment.objects.all()
